module SessionsHelper

  # Logs in the given user. 
  # Used in sessions_controller.rb
  # Used internally in current_user method
  def log_in(user)
    session[:user_id] = user.id
  end
  
  # Remembers a user in a persistent session. 
  # Used in sessions_controller.rb
  def remember(user)
    # In /models/users.rb
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
  
  # Returns true if the given user is the current user.
  def current_user?(user)
    user == current_user
  end

  # Returns the user corresponding to the remember token cookie.
  # Used internally in logged_in? method and log_out method
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      # In /models/users.rb
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  # Returns true if the user is logged in, false otherwise.
  # Used in _header.hmtl.erb and sessions_controller.rb
  def logged_in?
    !current_user.nil?
  end
  
  # Forgets a persistent session.
  # Used internally by log_out method
  def forget(user)
    # In /models/users.rb
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end
  
  # Logs out the current user.
  # Used in sessions_controller.rb
  def log_out
    # In /models/users.rb
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  # Redirects to stored location (or to the default).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end


end
