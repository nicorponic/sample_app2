# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name:  "AdMiN",
             email: "n1co_frago@hotmail.com",
             password:              "Admin!$",
             password_confirmation: "Admin!$",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)
             
User.create!(name:  "Nikolaos",
             email: "fragkopoulos.nikos@gmail.com",
             password:              "gr3123GR",
             password_confirmation: "gr3123GR",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)
             
             
users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end